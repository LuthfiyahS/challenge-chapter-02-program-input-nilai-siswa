module.exports = class User {
    constructor(props) {
        // props adalah object
        let { mapel, kodemapel } = props; // Destructor
        this.mapel = mapel;
        this.encryptedKode = this.#encrypt(kodemapel); 
    }

    // Private method pakai Encapsulation
    #encrypt = (kodemapel) => {
        return `pretend-this-is-an-encrypted-version-of-${kodemapel}`
    }

    #decrypt = () => {
        return this.encryptedKode.split(`pretend-this-is-an-encrypted-version-of-`)[1];
    }

    authenticate(mapel,kodemapel) {
        if(mapel === this.mapel && this.#decrypt() === kodemapel) return mapel === this.mapel && this.#decrypt() === kodemapel; // Will return true or false
        
    }
}