class Matapelajaran{
    constructor(mapel) { 
        if (this.constructor === Matapelajaran) {
            console.log(`Hai, kamu masuk ke mata pelajaran ${this.mapel}`)
        }
        this.mapel = mapel;
    }
    
    introduce() {
        console.log(`Hai, kamu masuk ke mata pelajaran ${this.mapel}`)
    }
}

const viewTerimakasih = Base => class extends Base { //Polymorphism
    viewClosingStatement() {
      console.log("Terimakasih Atas Perhatiannya Cikgu!")
    }
}
/*
let js = new Matapelajaran('JavaScript')
    js.introduce(); //error  karena ini abstract class coba buka komentar dan jalanin ini
    */
module.exports = {Matapelajaran,viewTerimakasih}