const Auth = require('./class/Auth');
const { NilaiSiswa, rl } = require('./class/NilaiSiswa');

let JavaScript = new Auth({
    mapel: "JavaScript",    //untuk input mapel
    kodemapel: "1"          //untuk input kode
})

let pel, kodemapel;
let session = 3;
console.log("Hai Kak👋\nSuttt aku kasih tau mapel sama kodenya ya tapi jangan bilang-bilang mama hihi, \nPelajaran JavaScript, Kodenya 1!\n");
const inputmapel = () => {
    rl.question("Pelajaran           : ", (ans) => {
        pel = ans
        inputkode();
    })
}

const inputkode = () => {
    rl.question("Kode Mata Pelajaran : ", (answer) => {
        rl.stdoutMuted = false;
        kodemapel = answer;
        const isAuthenticated = JavaScript.authenticate(pel, kodemapel);
        session--;
        console.log(`\nMata Pelajaran dan atau Kode Mata Pelajaran yang anda masukkan ${isAuthenticated ? "BENAR\n" : `SALAH!\nKesempatan (${session}x) \n`}`) // true
        if (isAuthenticated == true) {
            main();
        } else {
            if (session == 0) {
                console.log("Aplikasi akan terhenti, Silahkan mencoba beberapa saat lagi!");
                setTimeout(() => rl.close(), 3000);
            } else {
                inputmapel();
            }
        }
    })
}

inputmapel();
const main = () => {
    let js = new NilaiSiswa(JavaScript.mapel,'Luthfiyah');
    console.log("===================================================");
    js.introduce();
    console.log("\n==================INPUT DATA SISWA=================\n");
    console.log("Silahkan input dan beri nilai q jika ingin berhenti\n");
    js.inputdataSiswa();
}