const {Matapelajaran,viewTerimakasih} = require('./Matapelajaran');
const readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

class NilaiSiswa extends viewTerimakasih(Matapelajaran) { //inheritance
    constructor(mapel,tutor) { 
        super (mapel);
        this.tutor = tutor;
        this.dataSiswa = [];
    }

    // Override
    introduce() {
        super.introduce(); // Memanggil super class introduce.
        console.log(`Jumlah siswa dikelasmu saat ini ${this.dataSiswa.length} Cikgu ${this.tutor}, yuk input datanya !`);
    }

    inputdataSiswa() {
        let id, namasiswa, nilai;
        id = this.dataSiswa.length+1;

        const inputnama = ()=>{
            rl.question("Nama Siswa       : ", (ans) => {
                namasiswa = ans;       
                inputnilai();
            })
        }
        const inputnilai = ()=>{
            rl.question(`Nilai ${this.mapel} : `, (answer) => {
                if(answer=='q'){
                    console.log('\x1b[33mHelp\n\x1b[37mq : quit\n')
                    console.log("\n============DATA YANG DIINPUTKAN================");
                    console.table(this.dataSiswa)
                    this.tanyasorting();
                }else{
                    answer=+answer;
                    let validate = (typeof answer == 'number') && !isNaN(answer) && answer != '' && answer >=0 && answer <=100 && namasiswa!=''
                    if(validate){
                        nilai = answer;
                        let hasil;
                        if(nilai>=0 && nilai<75)hasil='TIDAK LULUS'
                        if(nilai>=75 && nilai<=100)hasil='LULUS'
                        this.dataSiswa.push({ 'no absen': id, nama: namasiswa, nilai: nilai , 'hasil ujian': hasil});
                        console.log('');
                        this.inputdataSiswa();
                    }else{
                        console.log("Inputan harus berupa angka 0 - 100")
                        inputnilai();
                    }
                }
            })
        }
        inputnama();
    }

    tanyasorting(){
        console.log('\nKamu sudah input semua data, ingin menampilkan data secara apa?')
        console.log('1. Ascending  (Terendah - Tertinggi)      2. Descending (Tertinggi - Terendah)')
        rl.question(`Masukan Pilihanmu (1/2) :`, (answer) => {
            if(answer=='1'){
                this.sortingasc()
                rl.close();
            }else if(answer=='2'){
                this.sortingdsc();
                rl.close();
            }else{
                this.tanyasorting()
            }
        })
    }

    sortingasc(){
        let len= this.dataSiswa.length;
        let a = this.dataSiswa;
        for(var i = len-1; i>=0; i--){
            for(var j = 1; j<=i; j++){
                if(a[j-1]['nilai'] >a[j]['nilai'] ){
                    var temp = a[j-1]
                    a[j-1] = a[j]
                    a[j] = temp
                }
            }
        }
        console.log("\n============DATA TERURUT ASCENDING================");
        console.table(this.dataSiswa)
        this.#output(len,a);
    }

    sortingdsc(){
        let len= this.dataSiswa.length;
        let a = this.dataSiswa;
        for(var i = len-1; i>=0; i--){
            for(var j = 1; j<=i; j++){
                if(a[j-1]['nilai'] <a[j]['nilai'] ){
                    var temp = a[j-1]
                    a[j-1] = a[j]
                    a[j] = temp
                }
            }
        }
        console.log("\n============DATA TERURUT DESCENDING================");
        console.table(this.dataSiswa)
        this.#output(len,a);
    }

    //private
    #output(len,a) {
        var tmp, avg = 0, lulus = 0, tidaklulus = 0;
        for (var i = len - 1; i >= 0; i--) {
            tmp = a[i].nilai;
            tmp >= 75 ? lulus += 1 : tidaklulus += 1;
            avg += tmp;
        }
        console.log("==========OUTPUT YANG DIHARAPKAN=============");
        console.log("=============================================");
        console.log("1. Nilai Tertinggi dan terendah");
        console.log('   Nilai \x1b[32mTertinggi : ', a[len - 1].nilai, ' dengan No Absen ',a[len - 1]['no absen'])
        console.log('   Nilai \x1b[31mTerendah  : ', a[0].nilai, ' dengan No Absen ',a[0]['no absen'])
        console.log("=============================================");
        console.log("2. Rata-rata nilai");
        console.log('   Nilai \x1b[34mRata-rata : ', avg, '/', len, ' = ', avg / len)
        console.log("=============================================");
        console.log("3. Jumlah Siswa Lulus dan Tidak Lulus (KKM 75)");
        console.log('   Jumlah Siswa \x1b[32mLULUS        : ', lulus, ' orang')
        console.log('   Jumlah Siswa \x1b[31mTIDAK LULUS  : ', tidaklulus, ' orang')
        console.log("=============================================", '\n');
        console.log("   Jumlah Siswa Keseluruhan  : ", len , ' orang\n');
        this.#closing();
    }

    //private
    #closing(){
        
                super.viewClosingStatement(); //Polymorphism
                setTimeout(() => rl.close(), 3000);
            
    }
}

module.exports = {NilaiSiswa,rl}